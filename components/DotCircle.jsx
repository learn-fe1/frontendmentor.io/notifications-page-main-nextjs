export default function DotCircle() {
  return (
    <div className="h-[10px] w-[10px] rounded-xl bg-red-700 inline-block mx-2"></div>
  );
}
