import Image from "next/image";

export default function MessageUser({ message }) {
  return (
    <div className="border-solid border-[#ECF0F1] border-2 p-3 ml-20xzrftrer mr-5 text-xl hover:bg-[#E5EFF9]">
      {message}
    </div>
  );
}
