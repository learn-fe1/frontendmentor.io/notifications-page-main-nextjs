export default function TopBar() {
  return (
    <div className="flex flex-row justify-between p-5">
      <div className="flex flex-row justify-center">
        <div className="text-xl font-bold">Notifications</div>
        <div
          className="mx-3 px-2 py-0 text-base flex items-center font-bold color text-white rounded-md"
          style={{ backgroundColor: "#0A327C" }}
        >
          3
        </div>
      </div>
      <div>
        <div className="hover:font-bold">Mark all as read</div>
      </div>
    </div>
  );
}
