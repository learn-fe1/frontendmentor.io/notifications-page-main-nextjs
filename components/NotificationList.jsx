import Image from "next/image";
import DotCircle from "./DotCircle";

export default function NotificationList({
  avatarSrc,
  authorName,
  postTitle,
  timeAgo,
  optionalParameter,
  isAlreadyRead,
}) {
  const containerStyle = {
    backgroundColor: isAlreadyRead ? "#F6FAFC" : undefined,
  };

  return (
    <div
      className="flex flex-row items-center px-5 py-2 my-1"
      style={containerStyle}
    >
      <Image
        src={avatarSrc}
        width={48}
        height={48}
        alt="Picture of the author"
      />
      <div className="flex flex-col px-2">
        <div className="text-small hover:selection:{authorName}:color">
          <span className="font-bold hover:text-[#002771]">{authorName}</span>
          <span>{postTitle}</span>
          <span className="font-bold text-[#656B75] hover:text-[#002771]">
            {optionalParameter}
          </span>
          {isAlreadyRead ? <DotCircle /> : null}
        </div>
        <div>{timeAgo}</div>
      </div>
    </div>
  );
}
