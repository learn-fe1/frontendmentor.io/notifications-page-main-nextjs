import TopBar from "@/components/TopBar";
import NotificationList from "@/components/NotificationList";
import MessageUser from "@/components/MessageUser";

export default function Home() {
  return (
    <div className="flex justify-center h-screen items-center">
      <div
        className="md:w-[800px] p-5 w-[500]"
        style={{ backgroundColor: "#FFFFFF" }}
      >
        <div className="flex flex-col">
          <TopBar />
          <NotificationList
            avatarSrc="/images/avatar-mark-webber.webp"
            authorName="Mark Webber"
            postTitle=" reacted to your recent post "
            optionalParameter="My first tournament today!"
            timeAgo="1m ago"
            isAlreadyRead="true"
          />
          <NotificationList
            avatarSrc="/images/avatar-angela-gray.webp"
            authorName="Angela Gray"
            postTitle=" followed you"
            optionalParameter=""
            timeAgo="5m ago"
            isAlreadyRead="true"
          />
          <NotificationList
            avatarSrc="/images/avatar-jacob-thompson.webp"
            authorName="Jacob Thompson"
            postTitle=" has joined your group "
            optionalParameter="Chess Club"
            timeAgo="1 day ago"
            isAlreadyRead="true"
          />
          <NotificationList
            avatarSrc="/images/avatar-rizky-hasanuddin.webp"
            authorName="Rizky Hasanuddin"
            postTitle=" sent you a private message"
            optionalParameter=""
            timeAgo="5 day ago"
          />
          <MessageUser message="Hello, thanks for setting up the Chess Club. I've been a member for a few weeks now and I'm already having lots of fun and improving my game." />
          <NotificationList
            avatarSrc="/images/avatar-kimberly-smith.webp"
            authorName="Kimberly Smith"
            postTitle=" commented on your picture"
            optionalParameter=""
            timeAgo="1 week ago"
          />
          <NotificationList
            avatarSrc="/images/avatar-nathan-peterson.webp"
            authorName="Nathan Peterson"
            postTitle=" reacted to your recent post "
            optionalParameter="5 end-game strategies to increase your win rate"
            timeAgo="2 week ago"
          />
          <NotificationList
            avatarSrc="/images/avatar-anna-kim.webp"
            authorName="Anna Kim"
            postTitle=" left the group "
            optionalParameter="Chess Club"
            timeAgo="2 week ago"
          />
        </div>
      </div>
    </div>
  );
}
